package selenide.pages.reserveapp.finalconfirm

import com.codeborne.selenide.SelenideElement
import com.codeborne.selenide.Selenide.`$`

abstract class FinalConfirmPageBase {

    protected fun ErrorCheckResult(): SelenideElement {
        return `$`("#errorcheck_result")
    }

    protected fun ReturnToCheckInfo(): SelenideElement {
        return `$`("#returnto_checkInfo")
    }
}
