package selenide.helpers

import com.codeborne.selenide.Configuration
import com.codeborne.selenide.WebDriverRunner
import org.openqa.selenium.Proxy
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.events.EventFiringWebDriver
import selenide.helpers.browsers.Chrome.getChromeCapabilities
import selenide.helpers.browsers.Chrome.getChromeDriver
import selenide.helpers.browsers.Edge.getEdgeCapabilities
import selenide.helpers.browsers.Edge.getEdgeDriver
import selenide.helpers.browsers.Firefox.getFirefoxCapabilities
import selenide.helpers.browsers.Firefox.getFirefoxDriver
import selenide.helpers.browsers.IE.getIECapabilities
import selenide.helpers.browsers.IE.getIEDriver
import selenide.helpers.browsers.Safari.getSafariCapabilities
import selenide.helpers.browsers.Safari.getSafariDriver
import java.net.URL

object BrowserUtils {

    val USE_PROXY_FLAG = false
    private val USE_HUB_SERVER = false
    val USE_CHROME_HEADLESS = false

    // private val DEFAULT_PROXY_SERVER: String = "http://localhost:8888"
    private val DEFAULT_PROXY_SERVER: String = "http://192.168.0.6:8888"

    // private val SELENIUM_DEFAULT_HUB_SERVER: String = "http://localhost:4444/wd/hub"
    private val SELENIUM_DEFAULT_HUB_SERVER: String = "http://192.168.0.6:4444/wd/hub"

    private val DRIVER_ROOT_PATH = "./out/production/resources/drivers"

    private val hubServer: URL = getSeleniumHubServer()
    val proxyServer: URL = getProxyServer()

    /*
        各種ブラウザーを起動する
     */
    fun launchBrowser(browserType: BrowserType = BrowserType.CHROME): WebDriver {
        // WebDriverを起動する
        val driver = getWebDriver(browserType, USE_HUB_SERVER)

        // WebDriverEventListenerを設定する
        val efWebDriver = EventFiringWebDriver(driver)
        val listener = EventListener()
        efWebDriver.register(listener)
        WebDriverRunner.setWebDriver(efWebDriver)

        // WebDriverの動作設定を行う
        Configuration.timeout = 6000
        Configuration.browserSize = "1280x800"
        Configuration.holdBrowserOpen = false
        // Configuration.startMaximized = true
        // Configuration.baseUrl = ""

        return efWebDriver
    }

    fun closeBrowser(driver: WebDriver?) {
        driver?.quit()
    }

    /*
        ChromeとIEのプロキシー設定を得る
     */
    fun getProxy(): Proxy {

        val proxy = Proxy()
        val url = proxyServer.toString()
        proxy.setHttpProxy(url)
                .setFtpProxy(url)
                .setSslProxy(url)

        return proxy
    }

    /*
        任意のキー名の環境変数を取得する
     */
    private fun getEnv(name: String, defaultValue: String): String = if (System.getenv(name).isNullOrEmpty()) defaultValue else System.getenv(name)

    /*
        Selenium GridのHubサーバーのURLを取得する
     */
    private fun getSeleniumHubServer(name: String = "QA_SELENIUM_HUB_SERVER"): URL {
        val url: String = getEnv(name, SELENIUM_DEFAULT_HUB_SERVER)
        return URL(url)
    }

    /*
        ProxyサーバーのURLを取得する
     */
    private fun getProxyServer(name: String = "QA_PROXY_SERVER"): URL {
        val url: String = getEnv(name, DEFAULT_PROXY_SERVER)
        return URL(url)
    }

    /*
        WebDriverファイルのある場所のフルパスを得る
     */
    fun getDriverLocation(browserType: BrowserType): String {

        var operatingSystem = "windows"
        var executableFile = "chromedriver.exe"
        var browserFolder = "chrome"

        // Platformの判定
        var machineBits = "32bit"

        when (browserType) {
            BrowserType.CHROME -> {
                browserFolder = "chrome"
                executableFile = "chromedriver.exe"
                if (CommonUtils.isMac()) {
                    machineBits = "64bit"
                }
            }
            BrowserType.SMART_PHONE -> {
                browserFolder = "chrome"
                executableFile = "chromedriver.exe"
                if (CommonUtils.isMac()) {
                    machineBits = "64bit"
                }
            }
            BrowserType.FIREFOX -> {
                browserFolder = "firefox"
                executableFile = "geckodriver.exe"
                machineBits = "64bit"
            }
            BrowserType.IE -> {
                browserFolder = "ie"
                executableFile = "IEDriverService.exe"
            }
            BrowserType.EDGE -> {
                browserFolder = "edge"
                executableFile = "MicrosoftWebDriver.exe"
                machineBits = "64bit"
            }
            else -> {
                // TODO: 指定されたブラウザーの種別が存在しない場合
            }
        }

        // macOSの時は「.exe」の拡張子を削除する
        if (CommonUtils.isMac()) {
            operatingSystem = "mac"
            executableFile = executableFile.replace(".exe", "")
        }

        return "$DRIVER_ROOT_PATH/$browserFolder/$operatingSystem/$machineBits/$executableFile"
    }

    /*
        各種WebDriverのインスタンスを得る
     */
    // RemoteWebDriver(hubServer, capabilities)
    private fun getWebDriver(browserType: BrowserType, isRemote: Boolean = false): WebDriver {
        when (browserType) {
            BrowserType.CHROME, BrowserType.SMART_PHONE -> {
                val caps = getChromeCapabilities(browserType)
                return getChromeDriver(caps)
            }
            BrowserType.FIREFOX -> {
                val caps = getFirefoxCapabilities()
                return getFirefoxDriver(caps)
            }
            BrowserType.IE -> {
                val caps = getIECapabilities()
                return getIEDriver(caps)
            }
            BrowserType.EDGE -> {
                val caps = getEdgeCapabilities()
                return getEdgeDriver(caps)
            }
            BrowserType.SAFARI -> {
                val caps = getSafariCapabilities()
                return getSafariDriver(caps)
            }
        }
    }
}