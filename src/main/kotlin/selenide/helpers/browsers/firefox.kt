package selenide.helpers.browsers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.firefox.GeckoDriverService
import org.openqa.selenium.remote.DesiredCapabilities
import selenide.helpers.BrowserType
import selenide.helpers.BrowserUtils
import java.io.File

object Firefox {
    // DeveloperEdition: /Applications/FirefoxDeveloperEdition.app/Contents/MacOS/firefox-bin
    val BINARY_PATH: String = "/Applications/Firefox.app/Contents/MacOS/firefox-bin"

    /*
        Firefox用のWebDriverを初期化する
    */
    fun getFirefoxDriver(caps: DesiredCapabilities): WebDriver {
        val service = GeckoDriverService.Builder()
                .usingDriverExecutable(File(BrowserUtils.getDriverLocation(BrowserType.FIREFOX)))
                .usingAnyFreePort()
                .build()

        val options = FirefoxOptions()
        options.merge(caps)
        options.setBinary(BINARY_PATH)

        return FirefoxDriver(service, options)
    }

    /*
        Firefox用のDesiredCapabilitiesを取得する
     */
    fun getFirefoxCapabilities(): DesiredCapabilities {

        val caps = DesiredCapabilities.firefox()
        caps.setCapability("marionette", true)
        caps.setCapability(FirefoxDriver.PROFILE, createProfile(BrowserUtils.USE_PROXY_FLAG))

        return caps
    }

    /*
        FirefoxのProfile(プロキシー設定を含む)を取得する
     */
    private fun createProfile(useProxy: Boolean = false): FirefoxProfile {

        val profile = FirefoxProfile()
        if (useProxy) {
            val hostname = BrowserUtils.proxyServer.host
            val port = BrowserUtils.proxyServer.port
            // Configures the same proxy for all variants
            profile.setPreference("network.proxy.type", 1)
            profile.setPreference("network.proxy.http", hostname)
            profile.setPreference("network.proxy.http_port", port)
            profile.setPreference("network.proxy.ssl", hostname)
            profile.setPreference("network.proxy.ssl_port", port)
            // profile.setPreference("network.proxy.socks", "")
            // profile.setPreference("network.proxy.socks_port", 0)
            // profile.setPreference("network.proxy.ftp", "")
            // profile.setPreference("network.proxy.ftp_port", 0)
            // profile.setPreference("network.proxy.no_proxies_on", "localhost, 127.0.0.1")
        }

        return profile
    }
}