package selenide.helpers.browsers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.safari.SafariDriver
import org.openqa.selenium.safari.SafariDriverService
import org.openqa.selenium.safari.SafariOptions
import selenide.helpers.BrowserUtils

object Safari {
    /*
        Mac Safari用のWebDriverを初期化する
     */
    fun getSafariDriver(caps: DesiredCapabilities): WebDriver {
        val service = SafariDriverService.Builder()
                //.usingDriverExecutable(File(""))
                //.usingTechnologyPreview(false)
                .usingTechnologyPreview(true) // enable Technology Preview, but it's deprecated
                .usingAnyFreePort()
                .build()

        val options = SafariOptions()
        options.useCleanSession(true)
        // options.setProxy(BrowserUtils.getProxy()) // 無視される？

        return SafariDriver(service, options)
    }

    /*
        Mac SafariのDesiredCapabilitiesを得る
     */
    fun getSafariCapabilities(): DesiredCapabilities {

        val caps = DesiredCapabilities.safari()

        // TODO: Safariのオプションを調べる
        caps.setCapability("ensureCleanSession",true)

        return caps
    }
}