package selenide.helpers.browsers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.edge.EdgeDriverService
import org.openqa.selenium.edge.EdgeOptions
import org.openqa.selenium.remote.DesiredCapabilities
import selenide.helpers.BrowserType
import selenide.helpers.BrowserUtils
import java.io.File

object Edge {
    /*
        Microsoft Edge用のWebDriverを初期化する
     */
    fun getEdgeDriver(caps: DesiredCapabilities): WebDriver {
        val service = EdgeDriverService.Builder()
                .usingDriverExecutable(File(BrowserUtils.getDriverLocation(BrowserType.EDGE)))
                .usingAnyFreePort()
                .build()

        val options = EdgeOptions()

        return EdgeDriver(service, options)
    }

    /*
        Microsoft Edge用のDesiredCapabilitiesを取得する
     */
    fun getEdgeCapabilities(): DesiredCapabilities {

        val caps = DesiredCapabilities.edge()

        // TODO: Edgeのオプションを調べる
        // EdgeではProxyなどの設定がコードから変更出来ないようです
        /*
        caps.getCapabilities(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true)
        caps.getCapabilities(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION,true)
        caps.getCapabilities(InternetExplorerDriver.IGNORE_ZOOM_SETTING, false)
        // caps.getCapabilities(InternetExplorerDriver.IE_USE_PER_PROCESS_PROXY, true)
        if (USE_PROXY_FLAG) {
            caps.getCapabilities(CapabilityType.PROXY, getProxy())
        }
        */

        return caps
    }
}