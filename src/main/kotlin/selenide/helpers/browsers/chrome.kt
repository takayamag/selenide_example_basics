package selenide.helpers.browsers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeDriverService
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import selenide.helpers.BrowserType
import selenide.helpers.BrowserUtils
import java.io.File
import java.util.HashMap

object Chrome {
    // Canary: /Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary
    val BINARY_PATH: String = "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome"
    val DEVICE_NAME: String = "Nexus 5"

    /*
        Google Chrome用のWebDriverを初期化する
     */
    fun getChromeDriver(caps: DesiredCapabilities): WebDriver {
        val service = ChromeDriverService.Builder()
                .usingDriverExecutable(File(BrowserUtils.getDriverLocation(BrowserType.CHROME)))
                .usingAnyFreePort()
                .build()

        val options = ChromeOptions()
        options.merge(caps)

        options.setBinary(BINARY_PATH)
        if (BrowserUtils.USE_CHROME_HEADLESS) {
            options.addArguments("headless")
        }

        return ChromeDriver(service, options)
    }

    /*
        Google Chrome用のDesiredCapabilitiesを取得する
     */
    fun getChromeCapabilities(browserType: BrowserType): DesiredCapabilities {

        val caps = DesiredCapabilities.chrome()
        if (BrowserUtils.USE_PROXY_FLAG) {
            caps.setCapability(CapabilityType.PROXY, BrowserUtils.getProxy())
        }
        if (browserType == BrowserType.SMART_PHONE) {
            caps.setCapability(ChromeOptions.CAPABILITY, getSmartPhoneOption())
        }

        return caps
    }

    /*
        Google ChromeのSPシミュレーターのオプションをHashMap形式で取得する
     */
    private fun getSmartPhoneOption(): HashMap<String, Any> {

        val mobileEmulation = HashMap<String, String>()
        mobileEmulation.put("deviceName", DEVICE_NAME)

        val mobileOptions = HashMap<String, Any>()
        mobileOptions.put("mobileEmulation", mobileEmulation)

        return mobileOptions
    }
}