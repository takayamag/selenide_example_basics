package selenide.features

import selenide.pages.reserveapp.inputform.InputPage
import com.codeborne.selenide.Selenide.*
import selenide.helpers.BrowserUtils.launchBrowser
import selenide.helpers.BrowserUtils.closeBrowser
import selenide.helpers.BrowserType
import org.junit.Assert.assertEquals
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.*
import org.junit.*
import org.openqa.selenium.WebDriver

class SelenideSampleSpec {

    private lateinit var driver: WebDriver

    @Before
    fun setUp() {
        driver = launchBrowser(BrowserType.SMART_PHONE)
    }

    @After
    fun tearDown() {
        closeBrowser(driver)
    }

    @Test
    fun 正常系確認() {
        val inputPage = open("http://example.selenium.jp/reserveApp/", InputPage::class.java)
        inputPage.reserveYear = "2017"
        inputPage.reserveMonth = "12"
        inputPage.reserveDay = "10"
        inputPage.reserveTerm = "2"
        inputPage.setBreakfastOn()
        inputPage.guestName = "東京太郎"
        inputPage.isPlanA = false
        inputPage.isPlanB = false

        val checkPage = inputPage.clickGotoNext()
        assertThat(checkPage.errorCheckResult, `is`(""))
        assertThat(checkPage.dateFrom, `is`("2017年12月10日"))
        assertThat(checkPage.dateTo, `is`("2017年12月12日"))
        assertThat(checkPage.daysCount, `is`("2"))
        assertThat(checkPage.headcount, `is`("1"))
        assertThat(checkPage.bfOrder, `is`("あり"))
        assertThat(checkPage.guestName, `is`("東京太郎"))
        assertThat(checkPage.price, `is`("17750"))

        val finalPage = checkPage.doCommit()
        assertThat(finalPage.errorCheckResult, `is`(""))
    }

    @Test
    fun 三ヶ月以上先の予約はできない() {
        val inputPage = open("http://example.selenium.jp/reserveApp/", InputPage::class.java)
        inputPage.reserveYear = "2020"
        inputPage.reserveMonth = "12"
        inputPage.reserveDay = "24"
        inputPage.reserveTerm = "2"
        inputPage.setBreakfastOn()
        inputPage.guestName = "東京 太郎"
        inputPage.isPlanA = true
        inputPage.isPlanB = true

        val checkPage = inputPage.clickGotoNext()
        assertEquals("宿泊日には、3ヶ月以内のお日にちのみ指定できます。", checkPage.errorCheckResult)
    }
}