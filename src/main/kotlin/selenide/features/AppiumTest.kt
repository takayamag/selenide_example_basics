package selenide.features

import io.appium.java_client.ios.IOSDriver
import selenide.helpers.AppiumUtils
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.openqa.selenium.WebElement

class AppiumTestTest{

    var driver: IOSDriver<WebElement>? = null

    @Before
    fun setUp() {
        driver = AppiumUtils.launchIOSDriver()
    }

    @After
    fun tearDown() {
        AppiumUtils.closeIOSDriver(driver)
    }

    @Test
    fun 正常系確認() {
        /*
        val context = driver?.context
        driver?.context("NATIVE_APP")
        driver?.findElement(By.id("com.android.chrome:id/terms_accept"))?.click()
        driver?.findElement(By.id("com.android.chrome:id/negative_button"))?.click()
        driver?.context(context)
        */

        driver?.get("https://www.yahoo.co.jp/")
        System.out.println(driver?.currentUrl)
    }
}